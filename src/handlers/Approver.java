package handlers;

import common.Type;

public abstract class Approver {

    protected Approver next;

    /**
     * If needed, be free to change signature of abstract methods.
     */
    public void approve(int id, double cost, Type type) {
        if (canApprove(id, cost, type)) {
            System.out.println(getClass().getSimpleName() + " approved purchase with id " + id + " that costs " + cost);
            return;
        }

        System.out.println("Purchase with id " + id + " needs approval from higher position than " + getClass().getSimpleName() + ".");
        next.approve(id, cost, type);
    }
    protected abstract boolean canApprove(int id, double cost, Type type);

    /**
     * Method used for registering next approver level.
     * DO NOT CHANGE IT.
     */
    public Approver registerNext(Approver next) {
        this.next = next;
        return next;
    }
}
