package handlers;

import common.Type;

/**
 * //TODO - If needed, validate logic and if possible optimize code.
 */
public class Director extends Approver {

    @Override
    protected boolean canApprove(int id, double cost, Type type) {
        boolean result = false;
        if ((type == Type.CONSUMABLES && cost <= 500) ||
                (type == Type.CLERICAL && cost <= 1000) ||
                (type == Type.GADGETS && cost <= 1500) ||
                (type == Type.GAMING && cost <= 3500) ||
                (type == Type.PC && cost <= 6000))  {
            result = true;
        }

        return result;

    }
}
