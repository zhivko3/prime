package handlers;

import common.Type;


/**
 * //TODO - Implement approval implementation for VicePresident level
 */
public class VicePresident extends Approver{
    @Override
    protected boolean canApprove(int id, double cost, Type type) {
        boolean result = false;
        if ((type == Type.CONSUMABLES && cost <= 700) ||
                (type == Type.CLERICAL && cost <= 1500) ||
                (type == Type.GADGETS && cost <= 2000) ||
                (type == Type.GAMING && cost <= 4500) ||
                (type == Type.PC && cost <= 6500))  {
            result = true;
        }

        return result;

    }
}
